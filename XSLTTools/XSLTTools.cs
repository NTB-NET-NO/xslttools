﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;

namespace NTBNormalizer
{
    public class XSLTTools
    {

        public static string SportCalculateTimeFromWinner(string WinnerTime, string CurrentTime)
        {
         

            // If two of the three parameters are empty we'll return empty string
            if ((WinnerTime == "") || (CurrentTime == ""))
            {
                return "";
            }

            if (WinnerTime == "")
            {
                return "";
            } else if (CurrentTime == "") {
                return "";
            }
            

            
            DateTime dtWinner = new DateTime();
            string home = dtWinner.Day.ToString();



            string strDelemitter = "h:.,";
            char[] chrDelemitter = strDelemitter.ToCharArray();
            string[] strSplittedWinnerTime = null;
            string[] strSplittedCurrentTime = null;

            strSplittedWinnerTime = WinnerTime.Split(chrDelemitter);
            strSplittedCurrentTime= CurrentTime.Split(chrDelemitter);


            int intAddMilliseconds = 0;
            if (strSplittedWinnerTime.Length == 4) {
                if (strSplittedWinnerTime[3].Length == 2)
                {
                    intAddMilliseconds = 100;
                }
                else
                {
                    intAddMilliseconds = 10;
                }
                int intHour = Convert.ToInt32(strSplittedWinnerTime[0]);
                int intMinutes = Convert.ToInt32(strSplittedWinnerTime[1]);
                int intSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[3]);
                

                int intCurrentHour = Convert.ToInt32(strSplittedCurrentTime[0]);
                int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[1]);
                int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);
                int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[3]);

                
                int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    // intMilliSecondsBack += 10;
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                    intCurrentSeconds -= 1;

                }

                int intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intSecondsBack = intCurrentSeconds - intSeconds;

                    intCurrentMinutes -= 1;
                }

                int intMinutesBack = intCurrentMinutes - intMinutes;
                if (intMinutesBack < 0)
                {
                    intCurrentMinutes += 60;
                    intMinutesBack = intCurrentMinutes - intMinutes;

                    intCurrentHour -= 1;
                }

                int intHoursBack = intCurrentHour - intHour;
                if (intHoursBack < 0)
                {
                    // intHoursBack = 1;
                }
                string strHours = intHoursBack.ToString();
                string strMinutes = intMinutesBack.ToString();
                string strSeconds = intSecondsBack.ToString();
                string strMilliSeconds = intMilliSecondsBack.ToString();


                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                if (intHoursBack == 0)
                {


                    string strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn == "0.00,0")
                    {
                        strReturn = WinnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                        return strReturn;
                    }

                    return strReturn;

                }
                else
                {
                    
                    string strReturn = strHours + "." + strMinutes + "." + strSeconds + "," + strMilliSeconds;


                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn == "0.00.00,0")
                    {
                        strReturn = WinnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                        return strReturn;
                    }

                    
                    return strReturn;

                }

                
            }
            else if (strSplittedWinnerTime.Length == 3)
            {
                if (strSplittedWinnerTime[2].Length == 2)
                {
                    intAddMilliseconds = 100;
                }
                else
                {
                    intAddMilliseconds = 10;
                }
                int intMinutes = Convert.ToInt32(strSplittedWinnerTime[0]);
                int intSeconds = Convert.ToInt32(strSplittedWinnerTime[1]);
                int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[0]);
                int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[1]);
                int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);


                int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intCurrentSeconds-= 1;

                    // Doing the calculations again
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                }

                int intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intCurrentMinutes-= 1;

                    intSecondsBack = intCurrentSeconds - intSeconds;
                }

                if (strSplittedCurrentTime.Length == 4)
                {
                    intCurrentMinutes += 60;
                }

                int intMinutesBack = intCurrentMinutes - intMinutes;
                


                string strMinutes = intMinutesBack.ToString();
                string strSeconds = intSecondsBack.ToString();
                string strMilliSeconds = intMilliSecondsBack.ToString();

                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                string strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

                

                if (strReturn == "0.00,0") {
                    return WinnerTime.Replace(".", ",").Replace(":", "."); 
                }
                
                return strReturn;
            }



            return WinnerTime.Replace(".", ",").Replace(":", "."); 
            
        }



        public static string SportCalculateTimeFromWinnerWithRank(string WinnerTime, string CurrentTime, string Rank)
        {

            // If two of the three parameters are empty we'll return empty string
            if ((WinnerTime == "") || (CurrentTime == ""))
            {
                return "";
            }

            if (WinnerTime == "")
            {
                return "";
            } else if (CurrentTime == "") {
                return "";
            }
            else if (Rank == "")
            {
                return "";
            }


            
            DateTime dtWinner = new DateTime();
            string home = dtWinner.Day.ToString();



            string strDelemitter = "h:.,";
            char[] chrDelemitter = strDelemitter.ToCharArray();
            string[] strSplittedWinnerTime = null;
            string[] strSplittedCurrentTime = null;

            strSplittedWinnerTime = WinnerTime.Split(chrDelemitter);
            strSplittedCurrentTime= CurrentTime.Split(chrDelemitter);


            int intAddMilliseconds = 0;
            if (strSplittedWinnerTime.Length == 4) {
                if (strSplittedWinnerTime[3].Length == 2)
                {
                    intAddMilliseconds = 100;
                }
                else
                {
                    intAddMilliseconds = 10;
                }
                int intHour = Convert.ToInt32(strSplittedWinnerTime[0]);
                int intMinutes = Convert.ToInt32(strSplittedWinnerTime[1]);
                int intSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[3]);
                

                int intCurrentHour = Convert.ToInt32(strSplittedCurrentTime[0]);
                int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[1]);
                int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);
                int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[3]);

                
                int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    // intMilliSecondsBack += 10;
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                    intCurrentSeconds -= 1;

                }

                int intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intSecondsBack = intCurrentSeconds - intSeconds;

                    intCurrentMinutes -= 1;
                }

                int intMinutesBack = intCurrentMinutes - intMinutes;
                if (intMinutesBack < 0)
                {
                    intCurrentMinutes += 60;
                    intMinutesBack = intCurrentMinutes - intMinutes;

                    intCurrentHour -= 1;
                }

                int intHoursBack = intCurrentHour - intHour;
                if (intHoursBack < 0)
                {
                    // intHoursBack = 1;
                }
                string strHours = intHoursBack.ToString();
                string strMinutes = intMinutesBack.ToString();
                string strSeconds = intSecondsBack.ToString();
                string strMilliSeconds = intMilliSecondsBack.ToString();


                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                if (intHoursBack == 0)
                {


                    string strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn == "0.00,0")
                    {
                        strReturn = WinnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                        return strReturn;
                    }

                    if (Rank == "2")
                    {
                        strReturn += " min. bak";
                    }
                    return strReturn;

                }
                else
                {
                    
                    string strReturn = strHours + "." + strMinutes + "." + strSeconds + "," + strMilliSeconds;


                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn == "0.00.00,0")
                    {
                        strReturn = WinnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                        return strReturn;
                    }

                    if (Rank == "2")
                    {
                        strReturn += " min. bak";
                    }
                    return strReturn;

                }

                
            }
            else if (strSplittedWinnerTime.Length == 3)
            {
                if (strSplittedWinnerTime[2].Length == 2)
                {
                    intAddMilliseconds = 100;
                }
                else
                {
                    intAddMilliseconds = 10;
                }
                int intMinutes = Convert.ToInt32(strSplittedWinnerTime[0]);
                int intSeconds = Convert.ToInt32(strSplittedWinnerTime[1]);
                int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[0]);
                int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[1]);
                int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);


                int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intCurrentSeconds-= 1;

                    // Doing the calculations again
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                }

                int intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intMinutes-= 1;

                    intSecondsBack = intCurrentSeconds - intSeconds;
                }

                if (strSplittedCurrentTime.Length == 4)
                {
                    intCurrentMinutes += 60;
                }

                int intMinutesBack = intCurrentMinutes - intMinutes;
                


                string strMinutes = intMinutesBack.ToString();
                string strSeconds = intSecondsBack.ToString();
                string strMilliSeconds = intMilliSecondsBack.ToString();

                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                string strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

                

                if (strReturn == "0.00,0") {
                    return WinnerTime.Replace(".", ",").Replace(":", "."); 
                }
                if (Rank == "2")
                {
                    strReturn += " min. bak";
                }
                return strReturn;
            }



            return WinnerTime.Replace(".", ",").Replace(":", "."); 
            

        }
    }
}